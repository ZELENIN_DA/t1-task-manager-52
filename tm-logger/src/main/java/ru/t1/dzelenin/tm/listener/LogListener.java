package ru.t1.dzelenin.tm.listener;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.api.service.ILoggerService;
import ru.t1.dzelenin.tm.dto.LogDto;
import ru.t1.dzelenin.tm.service.LoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import java.io.Serializable;

public class LogListener implements MessageListener {

    @NotNull
    final ILoggerService loggerService = new LoggerService();

    @Override
    @SneakyThrows
    public void onMessage(Message message) {
        if (!(message instanceof ObjectMessage)) return;
        @NotNull final Serializable entity = ((ObjectMessage) message).getObject();
        if (entity instanceof LogDto) loggerService.writeLog((LogDto) entity);
    }

}