package ru.t1.dzelenin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.dzelenin.tm.api.service.IConnectionService;
import ru.t1.dzelenin.tm.api.service.IPropertyService;
import ru.t1.dzelenin.tm.api.service.dto.ITaskDTOService;
import ru.t1.dzelenin.tm.api.service.dto.IUserDTOService;
import ru.t1.dzelenin.tm.dto.model.TaskDTO;
import ru.t1.dzelenin.tm.dto.model.UserDTO;
import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.exception.field.DescriptionEmptyException;
import ru.t1.dzelenin.tm.exception.field.IdEmptyException;
import ru.t1.dzelenin.tm.exception.field.NameEmptyException;
import ru.t1.dzelenin.tm.exception.field.UserIdEmptyException;
import ru.t1.dzelenin.tm.service.dto.TaskDTOService;
import ru.t1.dzelenin.tm.service.dto.UserDTOService;

import java.util.List;
import java.util.UUID;

public class TaskServiceTest {

    @NotNull
    private IUserDTOService userService;

    @NotNull
    private ITaskDTOService taskService;

    private String USER_ID;

    private String TASK_ID;

    private long INITIAL_SIZE;

    @Before
    public void init() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        userService = new UserDTOService(connectionService, propertyService);
        taskService = new TaskDTOService(connectionService);
        @NotNull final UserDTO user = userService.create("user", "user");
        USER_ID = user.getId();
        @NotNull final TaskDTO task = taskService.create(USER_ID, "test-1");
        TASK_ID = task.getId();
        INITIAL_SIZE = taskService.getCount();
    }

    @After
    public void end() {
        taskService.clear(USER_ID);
        userService.removeByLogin("user");
    }

    @Test
    public void create() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.create("", "test"));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(USER_ID, ""));
        taskService.create(USER_ID, "test");
        Assert.assertEquals(INITIAL_SIZE + 1, taskService.getCount());
    }

    @Test
    public void createWithDescription() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.create("", "test", "test"));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(USER_ID, "", "test"));
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.create(USER_ID, "test", ""));
        taskService.create(USER_ID, "test", "test");
        Assert.assertEquals(INITIAL_SIZE + 1, taskService.getCount());
    }

    @Test
    public void clear() {
        taskService.clear();
        Assert.assertEquals(0, taskService.getCount());
    }

    @Test
    public void findAll() {
        @NotNull final List<TaskDTO> tasksAll = taskService.findAll();
        Assert.assertEquals(INITIAL_SIZE, tasksAll.size());
        @NotNull final List<TaskDTO> tasksOwnedUser1 = taskService.findAll(USER_ID);
        Assert.assertEquals(1, tasksOwnedUser1.size());
        @NotNull final List<TaskDTO> tasksOwnedUser2 = taskService.findAll(UUID.randomUUID().toString());
        Assert.assertEquals(0, tasksOwnedUser2.size());
    }

    @Test
    public void updateById() {
        Assert.assertThrows(UserIdEmptyException.class,
                () -> taskService.updateById("", TASK_ID, "test", "test"));
        Assert.assertThrows(IdEmptyException.class,
                () -> taskService.updateById(USER_ID, "", "test", "test"));
        Assert.assertThrows(NameEmptyException.class,
                () -> taskService.updateById(USER_ID, TASK_ID, "", "test"));
        @NotNull final String newName = "new name";
        @NotNull final String newDescription = "new description";
        taskService.updateById(USER_ID, TASK_ID, newName, newDescription);
        @Nullable final TaskDTO task = taskService.findOneById(TASK_ID);
        Assert.assertEquals(newName, task.getName());
        Assert.assertEquals(newDescription, task.getDescription());
    }

    @Test
    public void changeTaskStatusById() {
        @NotNull final Status newStatus = Status.COMPLETED;
        Assert.assertThrows(UserIdEmptyException.class,
                () -> taskService.changeStatusById("", TASK_ID, newStatus));
        Assert.assertThrows(IdEmptyException.class,
                () -> taskService.changeStatusById(USER_ID, "", newStatus));
        taskService.changeStatusById(USER_ID, TASK_ID, newStatus);
        @Nullable final TaskDTO task = taskService.findOneById(TASK_ID);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(newStatus, task.getStatus());
    }

    @Test
    public void findOneById() {
        @NotNull final String taskName = "test find by id";
        @NotNull final TaskDTO task = taskService.create(USER_ID, taskName);
        @NotNull final String taskId = task.getId();
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(""));
        Assert.assertNotNull(taskService.findOneById(taskId));
        Assert.assertEquals(taskName, taskService.findOneById(taskId).getName());
        Assert.assertNotNull(taskService.findOneById(USER_ID, taskId));
        Assert.assertEquals(taskName, taskService.findOneById(USER_ID, taskId).getName());
    }

    @Test
    public void existsById() {
        @NotNull final String taskName = "test exist by id";
        @NotNull final TaskDTO task = taskService.create(USER_ID, taskName);
        @NotNull final String taskId = task.getId();
        Assert.assertTrue(taskService.existsById(taskId));
        Assert.assertFalse(taskService.existsById(UUID.randomUUID().toString()));
    }

    @Test
    public void remove() {
        @NotNull final TaskDTO task = taskService.create(USER_ID, "test");
        @NotNull final String taskId = task.getId();
        taskService.remove(task);
        Assert.assertEquals(INITIAL_SIZE, taskService.getCount());
        taskService.add(task);
        taskService.remove(USER_ID, task);
        Assert.assertEquals(INITIAL_SIZE, taskService.getCount());
    }

    @Test
    public void removeById() {
        @NotNull final TaskDTO task = taskService.create(USER_ID, "test");
        @NotNull final String taskId = task.getId();
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(""));
        taskService.removeById(taskId);
        Assert.assertEquals(INITIAL_SIZE, taskService.getCount());
        taskService.add(task);
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(USER_ID, ""));
        taskService.removeById(USER_ID, taskId);
        Assert.assertEquals(INITIAL_SIZE, taskService.getCount());
    }

}
