package ru.t1.dzelenin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.dzelenin.tm.api.service.IConnectionService;
import ru.t1.dzelenin.tm.api.service.IPropertyService;
import ru.t1.dzelenin.tm.api.service.dto.IProjectDTOService;
import ru.t1.dzelenin.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.dzelenin.tm.api.service.dto.ITaskDTOService;
import ru.t1.dzelenin.tm.api.service.dto.IUserDTOService;
import ru.t1.dzelenin.tm.dto.model.ProjectDTO;
import ru.t1.dzelenin.tm.dto.model.TaskDTO;
import ru.t1.dzelenin.tm.dto.model.UserDTO;
import ru.t1.dzelenin.tm.exception.entity.TaskNotFoundException;
import ru.t1.dzelenin.tm.exception.field.IdEmptyException;
import ru.t1.dzelenin.tm.exception.field.UserIdEmptyException;
import ru.t1.dzelenin.tm.service.dto.ProjectDTOService;
import ru.t1.dzelenin.tm.service.dto.ProjectTaskDTOService;
import ru.t1.dzelenin.tm.service.dto.TaskDTOService;
import ru.t1.dzelenin.tm.service.dto.UserDTOService;

public class ProjectTaskServiceTest {

    @NotNull
    private IProjectTaskDTOService projectTaskService;

    @NotNull
    private IUserDTOService userService;

    @NotNull
    private IProjectDTOService projectService;

    @NotNull
    private ITaskDTOService taskService;

    private String USER_ID;

    private String PROJECT_ID;

    private String TASK_ID;

    @Before
    public void init() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        projectTaskService = new ProjectTaskDTOService(connectionService);
        userService = new UserDTOService(connectionService, propertyService);
        projectService = new ProjectDTOService(connectionService);
        taskService = new TaskDTOService(connectionService);
        @NotNull final UserDTO user = userService.create("user", "user");
        USER_ID = user.getId();
        @NotNull final ProjectDTO project = projectService.create(USER_ID, "project");
        PROJECT_ID = project.getId();
        @NotNull final TaskDTO task = taskService.create(USER_ID, "task");
        TASK_ID = task.getId();
    }

    @After
    public void end() {
        taskService.clear(USER_ID);
        projectService.clear(USER_ID);
        userService.removeByLogin("user");
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(UserIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject("", PROJECT_ID, TASK_ID));
        Assert.assertThrows(IdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID, "", TASK_ID));
        Assert.assertThrows(IdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, ""));
        Assert.assertThrows(TaskNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, "not_task_id"));

        projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, TASK_ID);
        @NotNull final TaskDTO task = taskService.findOneById(TASK_ID);
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(PROJECT_ID, task.getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        Assert.assertThrows(UserIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject("", PROJECT_ID, TASK_ID));
        Assert.assertThrows(IdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID, "", TASK_ID));
        Assert.assertThrows(IdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID, PROJECT_ID, ""));
        Assert.assertThrows(TaskNotFoundException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID, PROJECT_ID, "not_task_id"));
        projectTaskService.unbindTaskFromProject(USER_ID, PROJECT_ID, TASK_ID);
        @NotNull final TaskDTO task = taskService.findOneById(TASK_ID);
        Assert.assertNull(task.getProjectId());
    }

}
