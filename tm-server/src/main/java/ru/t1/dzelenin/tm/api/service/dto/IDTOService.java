package ru.t1.dzelenin.tm.api.service.dto;

import ru.t1.dzelenin.tm.api.repository.dto.IDTORepository;
import ru.t1.dzelenin.tm.dto.model.AbstractModelDTO;

public interface IDTOService<M extends AbstractModelDTO> extends IDTORepository<M> {
}