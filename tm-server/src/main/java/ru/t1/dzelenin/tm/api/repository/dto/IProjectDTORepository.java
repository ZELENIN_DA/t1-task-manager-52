package ru.t1.dzelenin.tm.api.repository.dto;

import ru.t1.dzelenin.tm.dto.model.ProjectDTO;

public interface IProjectDTORepository extends IUserOwnedDTORepository<ProjectDTO> {
}
