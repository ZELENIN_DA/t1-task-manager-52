package ru.t1.dzelenin.tm.api.service.model;


import ru.t1.dzelenin.tm.dto.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {
}
