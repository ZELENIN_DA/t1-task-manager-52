package ru.t1.dzelenin.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.dto.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}
