package ru.t1.dzelenin.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "taskmanager.task")
public final class TaskDTO extends AbstractUserOwnedModelDTO {

    private static final long serialVersionUID = 1;

    @Nullable
    @Column(name = "project_id", insertable=false, updatable=false)
    private String projectId = null;

}